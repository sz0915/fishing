package cn.tedu.game;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Net {

	//属性
	int x,y;
	
	BufferedImage netImage;	//网图与宽高
	int width;
	int height;
	
	//置标：控制网显示与否
	boolean show;
	
	//构造方法(new一个对象出来的时候，它的初始状态和初始参数
	public Net(){
		//当窗口初始化，鼠标未进入窗口，网默认在x，y的位置
		this.x = 400;
		this.y = 240;
		//初始图片 net_1.png
		File f = new File("images/net_1.png");
		try {
			this.netImage = ImageIO.read(f);
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.width = netImage.getWidth();
		this.height = netImage.getHeight();
		//默认有网，出现窗口中部
		this.show = true;
	}
	
	
	
	
	
	
	
	//普通方法=================================================
	
	//改变大小方法(用户右键点击传参，1-7，7过完回到1。根据此线索拼接图片名称
	public void change(int power){
		File f = new File("images/net_" + power + ".png");
		try {
			this.netImage = ImageIO.read(f);	//图片改变，宽高重新获取
			this.width = netImage.getWidth();
			this.height = netImage.getHeight();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	//移动方法(用户移动鼠标，通过监听事件获取鼠标坐标，赋值给x,y
	public void move(int x, int y){
		this.x = x;
		this.y = y;
	}
	
	//抓鱼方法(拿网的坐标和鱼的坐标做比较，捕鱼范围内：鱼减少生命值（power决定
	public boolean catchFish(Fish fish, int power){
		int xDistance = this.x - fish.x;
		int yDistance = this.y - fish.y;
		if(xDistance>=0&&xDistance<fish.width && 
				yDistance>=0&&yDistance<fish.height){
			//被捕范围内
			fish.blood = fish.blood - power;
			
		}
		return fish.blood <= 0;	//返回true代表鱼没血了。false代表血>0
		
	}
	
	
}
