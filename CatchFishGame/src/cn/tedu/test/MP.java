package cn.tedu.test;

//实现线程有两种常见方式：继承Tread类    实现Runnable【必须重写run()方法来发任务
public class MP extends Thread{

	static int ticket = 20;
	
	@Override
	public void run() {
		// 发布任务或者指令的
		//System.out.println("请开始你的表演！");
		while(ticket>0){
			System.out.println(Thread.currentThread().getName() + "车站，" + 
					Thread.currentThread().getId() + "柜台，正在出售票" + ticket--);
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	//模拟卖票
	public static void main(String[] args){
		/*for(int i=0;i<20;i++){
			System.out.println("正在出售票" + i);
		}*/
		MP mp = new MP();
		mp.start();
		
		MP mp2 = new MP();
		mp2.start();
	}
	
	
}
