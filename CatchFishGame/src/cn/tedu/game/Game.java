package cn.tedu.game;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * @author BUG
 *	1、鱼打滚消失后的一定时间内，可以刷分！！
 *	2、练习：子弹<=0    画GameOver
 *	3、欢迎界面：利用定时器加载欢迎界面。2s后进入游戏
 */
public class Game extends JFrame{

	public static void main(String[] args) throws IOException{
		Game game = new Game();	//产生窗口
		game.setSize(800,480);
		game.setLocationRelativeTo(null);
		game.setDefaultCloseOperation(3);
		game.setResizable(false);
		//将面板镶嵌到窗口类
		Pool p = new Pool();
		game.add(p);
		
		
		game.setVisible(true);
		
		//action方法里面包含while死循环，所以无法继续执行下面代码。必须放后面
		p.action();
		
	}
	
}
//池子类
class Pool extends JPanel{

	BufferedImage bg;
	Fish[] fishes;
	Net net;
	
	//分数、捕鱼个数、炮弹数量（绘制在面板上方
	int score = 0;
	int count = 0;
	int buttle = 300;
	
	int power = 1;	//炮弹（也就是网的大小）
	
	//构造方法（背景图片在池子创建之初就应该确定
	public Pool() throws IOException{
		//1、设置背景图片
		File f = new File("images/bg.jpg");	//声明一个文件
		try {
			bg = ImageIO.read(f);//将文件转化图片
		} catch (IOException e) {
		}
		
		//2、设置鱼 9*2 + 1 + 1 = 20条
		fishes = new Fish[20];
		//为每条鱼赋予种类	i<(fishes.length-2)/2 +1 +1	鱼有11种
		for(int i=0;i<9;i++){
			fishes[i] = new Fish(i+1);	//1-1 2-2 3-3 ..
			fishes[i+9] = new Fish(i+1);	// 10-1 11-2 12-3 ..
		}
		//上面只为前18条鱼分配了种类，手动为剩余两条鱼赋值
		fishes[18] = new Fish(13);	//鲨鱼和水母只让出现一条
		fishes[19] = new Fish(14);
		
		//3、设置网(需要写一个鼠标监听事件，让网图受鼠标控制
		net = new Net();
		initMouseEvent();	//调用鼠标监听方法
		
	}
	
	//绘图方法(要想实现动画，需要源源不断的持续的绘图，也就是持续的repaint()
	@Override
	public void paint(Graphics g) {
		//1、画背景图片
		g.drawImage(bg, 0, 0, null);
		//2、画鱼
		for(int i=0;i<fishes.length;i++){
			Fish fish = fishes[i];
			g.drawImage(fish.nowFish, fish.x, fish.y, null);
		}
		//3、画网(net.show为true才画图，否则不画
		if(net.show){
			g.drawImage(net.netImage, net.x - net.width/2, net.y-net.height/2, null);
		}
		
		//4、画分数
		g.setColor(Color.yellow);
		g.setFont(new Font("楷体", 0, 25));
		g.drawString("分数：" + score , 50, 30);
		g.drawString("捕鱼个数：" + count , 180, 30);
		g.drawString("子弹：" + buttle , 350, 30);
		g.setColor(Color.red);
		g.setFont(new Font("楷体", 1, 28));
		g.drawString("power：" + power , 650, 30);
		
		//子弹为0则GameOver
		if(buttle <=0 ){
			g.setColor(Color.WHITE);
			g.setFont(new Font("楷体",2,60));
			g.drawString("大写的菜！", 250, 200);
		}
		
	}
	
	//监测20条鱼的行动
	public void action(){
		for(int i=0;i<fishes.length;i++){
			//20条鱼种的每一条，都是一个new出来的fish对象，都要.start()
			fishes[i].start();
		}
		//每个时刻都要重绘repaint()
		while(true){
			repaint();
			try {
				Thread.sleep(100);//重绘：鱼100ms变一下位置，需要更快的频率重绘
			} catch (InterruptedException e) {
				
			}	
		}
	}
	
	
	//监测网、鼠标控制网图的方法
	public void initMouseEvent(){
		//变换光标
		this.setCursor(new Cursor(Cursor.HAND_CURSOR));
		//控制鼠标：先创建一个鼠标适配器MouseAdapter
		MouseAdapter ma = new MouseAdapter(){
			//怎么控制鼠标。或者说，鼠标的各种操作，你想绑定什么事件
			@Override
			public void mouseClicked(MouseEvent e) {
				// 鼠标左键(16)：抓鱼		右键(4)：变网
				if(e.getModifiers()==16){
					//子弹用尽，游戏结束
					if(buttle<=0){
						return;
					}
					buttle = buttle - power;	//网越大，子弹用得越快
					
					
					for(int i=0;i<fishes.length;i++){
						//每条鱼都有可能被抓，所以池子中的鱼都要参与判断
						if(net.catchFish(fishes[i], power)){
							//如果被抓：将鱼的isCatch属性变为true。run()方法自动让鱼打滚、消失
							fishes[i].isCatch = true;
							count++;	//捕鱼个数+1
							score = score + fishes[i].index*10;		//奖励得分
							buttle = buttle + fishes[i].index*2;	//奖励子弹
							
						}
					}
				}
				if(e.getModifiers()==4){
					//每点击一次右键，网的power + 1
					power++;
					if(power==8){
						power = 1;
					}
					net.change(power);
				}
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// 鼠标进入面板：网图出现
				net.show = true;
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// 鼠标离开面板：网图消失
				net.show = false;
			}

			@Override
			public void mouseMoved(MouseEvent e) {
				// 网跟随鼠标移动
				net.move(e.getX(), e.getY());
			}
			
		};
			
		//前往不要忘记：鼠标控制事件要【即时】监听！！	
		this.addMouseListener(ma);		//只监听按键点击等。。
		//用于接收组件上的鼠标移动事件的侦听器接口
		this.addMouseMotionListener(ma);
	}
	
	
	
	
	
	
}



