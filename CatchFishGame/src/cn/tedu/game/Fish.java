package cn.tedu.game;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;

//Thread线程类是为了	【实时】	监测鱼的行动
public class Fish extends Thread{

	String fishName;	//名字(图片名字
	int blood;		//生命值
	int x,y;		//坐标x,y
	int speed;		//速度
	int step;		//步数
	
	boolean isCatch;	//是否被抓（布尔值
	
	//鱼的10种游泳形态（图片数组
	BufferedImage[] swim = new BufferedImage[10];
	//鱼的被抓形态（图片数组，每种鱼被抓图片张数不一致
	BufferedImage[] catched;
	
	//鱼的当前形态！！（一张图
	BufferedImage nowFish;
	//鱼的种类
	int index;
	
	//鱼的图片的长和宽
	int width;
	int height;
	
	//========================================================
	
	//构造方法(通过new创建一条鱼，必须给它最基本的组成元素index(鱼的种类
	public Fish(int index) throws IOException{
		//1、生成一条鱼的时候控制鱼的种类。鱼的初始形态只有swim中的1-10
		this.index = index;		//鱼的种类
		this.fishName = "fish" + (index<10?"0":"") + index;
		//System.out.println(this.fishName);
		
		//2、确定鱼的种类后， 遍历swim中形态(形态从01开始的，所以 三目运算符  i=1
		for(int i=0;i<swim.length;i++){
			
			int xt = i + 1;	//形态（后缀
			String prefix = (xt==10?"":"0") + xt;	//形态从01开始的，所以 三目运
					
			//图片名字 = 鱼的名字 + 形态
			String imageName = "images/" + fishName + "_" + prefix + ".png";
			//System.out.println(imageName);
			//File + ImageIO = BufferIamge
			File f = new File(imageName);
			//将获取到的鱼的10种形态保存到，swim图片数组里面，等待被调用
			swim[i] = ImageIO.read(f);	//先获取图片名字，在回去图片，最后填充			
		}
		
		//3、根据鱼的种类确定被抓的图片的张树
		if(index<=7){
			catched = new BufferedImage[2];
		}else{
			catched = new BufferedImage[4];
		}
		//4、为被捕的图片赋值（拼接图片名称 + File + ImageIO
		for(int i=0;i<catched.length;i++){
			int xt = i + 1;
			String prefix = "0" + xt;	//被捕形态最大是4，所以不需要三目判断
			//图片名字 = 鱼的名字 + 形态
			String imageName = "images/" + fishName + "_catch_" + prefix + ".png";
			File f = new File(imageName);
			catched[i] = ImageIO.read(f);
		}
		
		//5、当前形态(第一次出现是swim中的第1形态
		nowFish = swim[0];
		//图片宽和高
		this.width = nowFish.getWidth();
		this.height = nowFish.getHeight();
		
		//鱼的xy坐标
		Random random = new Random();
		this.x = random.nextInt(800);	//生成0-800之间的整数作为x的值
		this.y = random.nextInt(480);
		//6、设置鱼的血量、速度
		this.blood = random.nextInt(index * 5 + 4);
		this.speed = random.nextInt(10) + 3;
		
		
	}
	
	//=================================================
	//辅助方法1：被捕打滚消失
	public void turnOver(){
		//为了良好的用户体验，被捕动画需要存在2s左右，大概被捕4遍
		for (int j = 0; j < 4; j++) {
			//被抓图片来回循环（并且，播放图片要有短暂的合适的间隔时间
			for (int i = 0; i < catched.length; i++) {
				//被捕图片依次赋给当前图片
				nowFish = catched[i];
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} 
		}
	}
	//辅助方法2：重新出场
	public void getOut(){
		//因为要保证池子鱼的种类*2  鲨鱼和水母*1  所以抓什么补什么
		Random random = new Random();
		this.blood = random.nextInt(index * 5 + 4);
		this.speed = random.nextInt(10) + 3;
		//出场位置（x=800  y坐标0-480都可以
		this.x = 800;
		this.y = random.nextInt(480 - this.height);
		this.isCatch = false;	//被抓后，重新出场，就变为为被抓状态
	}
	//辅助方法3：游动
	public void move(){
		
		nowFish = swim[step%10];
		//步数+1
		this.step = this.step + 1;
		
		//x减小
		this.x = this.x - speed;
	}
	
	
	//重写run()方法，监测鱼的行动
	public void run(){
		while(true){
			//当鱼游到边界，或者被抓：翻滚、消失、重新出场
			if(this.x <= 0-this.width || this.isCatch== true){
				turnOver();
				getOut();
				this.isCatch = false;
			}else{
				move();	//没有被抓，则正常游动、行走
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	
	
}
