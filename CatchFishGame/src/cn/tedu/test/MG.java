package cn.tedu.test;

import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFrame;

public class MG {

	public static void main(String[] args){
		System.out.println("2s后将出现一个窗口！！");
		//定时器Timer。
		//可以给它一个命令，命令可以在一定延时后执行一次，也可同频率下反复执行
		Timer t = new Timer();
		t.schedule(new TimerTask(){

			@Override
			public void run() {
				JFrame jf = new JFrame();
				jf.setVisible(true);
				jf.setSize(400, 800);
			}
			
		}, 2000);
		
		
	}
	
}
